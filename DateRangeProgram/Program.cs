﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DateRangeProgram.Controllers;

namespace DateRangeProgram
{
    class Program
    {
        static void Main(string[] arguments)
        {
            ApplicationController applicationController = new ApplicationController();
            applicationController.AppStart(arguments);
        }
    }
}
