﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateRangeProgram.Exceptions
{
    public class WrongDateRangeException : Exception
    {
        public WrongDateRangeException()
        {
        }

        public WrongDateRangeException(string message)
            : base(message)
        {
        }
    }
}
