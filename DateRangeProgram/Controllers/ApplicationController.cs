﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateRangeProgram.Controllers
{
    public class ApplicationController
    {
        public void AppStart(string[] arguments)
        {
            //Create controller instances
            ValidationController validationController = new ValidationController();
            ConversionController conversionController = new ConversionController();
            DateRangeController dateRangeController = new DateRangeController();

            try
            {
                //Validate input parameters
                validationController.ValidateArguments(arguments);
               
                //Convert string arguments to DateTime objects
                DateTime[] dates = conversionController.ConvertArguments(arguments);
               
                //Validate dates
                validationController.ValidateDates(dates);

                //Generate date range
                string dateRange = dateRangeController.GetDateRange(dates);
                Console.WriteLine(dateRange);
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine(
                    $"Exception occured; Exception type: {exception.GetType()}; Exception message: {exception.Message}");
                Console.ReadKey();
                Environment.Exit(0);
            }
        }
    }
}
