﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateRangeProgram.Controllers
{
    public class DateRangeController
    {
        public string GetDateRange(DateTime[] dates)
        {
            //Get both dates from array
            DateTime startDate = dates[0];
            DateTime endDate = dates[1];

            //Return date range without similarities in dates
            return $"{startDate:d} - {endDate:d}";
        }
    }
}
