﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DateRangeProgram.Exceptions;

namespace DateRangeProgram.Controllers
{
    public class ValidationController
    {
        private const int ExpectedNumberOfArguments = 2;

        public void ValidateArguments(string[] arguments)
        {
            //Validate number of arguments
            if (!ValidateNumberOfArguments(arguments))
            {
                throw new ArgumentException("Number of arguments is not equal to 2, please check your input parameters and try again."); 
            }
        }

        public void ValidateDates(DateTime[] dates)
        {
            DateTime startDate = dates[0];
            DateTime endDate = dates[1];
            //Validate date range
            if (!ValidateDateRange(startDate, endDate))
            {
                throw new WrongDateRangeException("There was an error with date range, please check your dates and try again.");
            }
        }

        private bool ValidateNumberOfArguments(string[] arguments)
        { 
            return arguments.Length == ExpectedNumberOfArguments;
        }

        private bool ValidateDateRange(DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate) return false;

            return true;
        }
    }
}
