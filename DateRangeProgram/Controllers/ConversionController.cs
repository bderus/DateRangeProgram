﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DateRangeProgram.Exceptions;

namespace DateRangeProgram.Controllers
{
    public class ConversionController
    {
        private DateTime _dateTime;
        private string _dateCulture;

        public DateTime[] ConvertArguments(string[] arguments)
        {
            //Check date culture of input dates for further conversion
            CheckForDateCulture(arguments);
            if (Equals(_dateCulture, null))
            {
                throw new WrongDateFormatException("There was an error with date format, please check your dates and try again.");
            }

            //Convert string dates to DateTime objects
            DateTime[] inputDates = new DateTime[2];
            inputDates[0] = DateTime.ParseExact(arguments[0], CultureInfo.GetCultureInfo(_dateCulture).DateTimeFormat.ShortDatePattern,
                CultureInfo.GetCultureInfo(_dateCulture));
            inputDates[1] = DateTime.ParseExact(arguments[1], CultureInfo.GetCultureInfo(_dateCulture).DateTimeFormat.ShortDatePattern,
                CultureInfo.GetCultureInfo(_dateCulture));

            return inputDates;
        }

        private bool CheckForDateCulture(string[] arguments)
        {
            if (CheckForCurrentCulture(arguments))
            {
                _dateCulture = CultureInfo.CurrentUICulture.Name;
                return true;
            }

            return CheckForAllCultures(arguments);
        }

        private bool CheckForCurrentCulture(string[] arguments)
        {
            String currentCultureName = CultureInfo.CurrentUICulture.Name;

            bool isCurrentCulture = (DateTime.TryParseExact(arguments[0],
                                         CultureInfo.GetCultureInfo(currentCultureName).DateTimeFormat.ShortDatePattern,
                                         CultureInfo.InvariantCulture, DateTimeStyles.None, out _dateTime)
                                     &&
                                     DateTime.TryParseExact(arguments[1],
                                         CultureInfo.GetCultureInfo(currentCultureName).DateTimeFormat.ShortDatePattern,
                                         CultureInfo.InvariantCulture, DateTimeStyles.None, out _dateTime));

            return isCurrentCulture;
        }

        private bool CheckForAllCultures(string[] arguments)
        {
            foreach (CultureInfo culture in CultureInfo.GetCultures(CultureTypes.AllCultures))
            {
                if (DateTime.TryParseExact(arguments[0],
                        CultureInfo.GetCultureInfo(culture.Name).DateTimeFormat.ShortDatePattern,
                        CultureInfo.GetCultureInfo(culture.Name), DateTimeStyles.None, out _dateTime)
                    &&
                    DateTime.TryParseExact(arguments[1],
                        CultureInfo.GetCultureInfo(culture.Name).DateTimeFormat.ShortDatePattern,
                        CultureInfo.GetCultureInfo(culture.Name), DateTimeStyles.None, out _dateTime))
                {
                    _dateCulture = culture.Name;
                    return true;
                }
            }

            return false;
        }
    }
}
