﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using DateRangeProgram.Controllers;
using DateRangeProgram.Exceptions;

namespace program.Controllers.Tests
{
    [TestClass]
    public class ConversionControllerTests
    {
        [TestMethod]
        [ExpectedException(typeof(WrongDateFormatException))]
        public void TestIfWrongDateFormatThrowsException()
        {
            string testDate = "2017/15/03";
            string testDate2 = "2017/04/15";
            string[] testArguments = new[] { testDate, testDate2 };
            ConversionController testConversionController = new ConversionController();
            testConversionController.ConvertArguments(testArguments);
        }

        [TestMethod]
        public void TestIfConversionIsSuccessful()
        {
            string testDate = "2017/03/15";
            string testDate2 = "2017/04/15";
            string[] testArguments = new[] { testDate, testDate2 };
            ConversionController testConversionController = new ConversionController();
            DateTime[] receivedDates = testConversionController.ConvertArguments(testArguments);
            DateTime receivedStartDate = receivedDates[0];
            DateTime receivedEndDate = receivedDates[1];
            DateTime expectedStartDate = new DateTime(2017, 03, 15);
            DateTime expectedEndDate = new DateTime(2017, 04, 15);
            Assert.AreEqual(expectedStartDate, receivedStartDate);
            Assert.AreEqual(expectedEndDate, receivedEndDate);
        }
    }
}