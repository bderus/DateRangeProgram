﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using DateRangeProgram.Controllers;
using DateRangeProgram.Exceptions;

namespace program.Controllers.Tests
{
    [TestClass]
    public class ValidationControllerTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestIfLessThan2ArgumentsThrowsException()
        {
            string testDate = "2017/01/01";
            string[] testArguments = new[] { testDate };
            ValidationController testValidationController = new ValidationController();
            testValidationController.ValidateArguments(testArguments);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestIfMoreThan2ArgumentsThrowsException()
        {
            string testDate = "2017/01/01";
            string testDate2 = "2017/01/02";
            string testDate3 = "2017/01/03";
            string[] testArguments = new[] { testDate, testDate2, testDate3 };
            ValidationController testValidationController = new ValidationController();
            testValidationController.ValidateArguments(testArguments);
        }

        [TestMethod]
        [ExpectedException(typeof(WrongDateRangeException))]
        public void TestIfWrongRangeOfDatesThrowsException()
        {
            DateTime testStartDate = new DateTime(2017, 01, 01);
            DateTime testEndDate = new DateTime(2016, 01, 01);
            DateTime[] testArguments = new[] { testStartDate, testEndDate };
            ValidationController testValidationController = new ValidationController();
            testValidationController.ValidateDates(testArguments);
        }
    }
}