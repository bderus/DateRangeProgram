﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using DateRangeProgram.Controllers;

namespace program.Controllers.Tests
{
    [TestClass]
    public class DateRangeControllerTests
    {
        [TestMethod]
        public void TestIfDateRangeWithoutSimilaritiesIsCorrect()
        {
            DateTime testStartDate = new DateTime(2016, 03, 15);
            DateTime testEndDate = new DateTime(2017, 03, 15);
            DateTime[] testDates = new DateTime[] { testStartDate, testEndDate };
            string expectedDateRange = $"{testStartDate:d} - {testEndDate:d}";
            DateRangeController testDateRangeController = new DateRangeController();
            string receivedDateRange = testDateRangeController.GetDateRange(testDates);
            Assert.AreEqual(expectedDateRange, receivedDateRange);
        }
    }
}